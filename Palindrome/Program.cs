﻿using System;
using System.Numerics;

namespace Palindrome
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 10; i < 201; i++)
            {
                PerformOperations(i);
            }

            Console.ReadKey();
        }

        private static bool CheckValue(BigInteger num)
        {
            BigInteger temp = num;
            BigInteger rem, sum = 0;
            bool found = false;

            while (num > 0)
            {
                rem = num % 10; //for getting remainder by dividing with 10  
                num = num / 10; //for getting quotient by dividing with 10
                sum = sum * 10 + rem;  /*multiplying the sum with 10 and adding remainder*/

                if (temp == sum) //checking whether the reversed number is equal to entered number    
                {
                    found = true;
                }
            }

            return found;
        }

        private static void PerformOperations(BigInteger num)
        {
            bool found = false;

            int numberOfSteps = 0;
            BigInteger temp = num;

            while (!found)
            {
                if (Display(CheckValue(num), temp, numberOfSteps))
                {
                    return;
                }

                num = num + PerformInvert(num);
                numberOfSteps++;

                if (numberOfSteps > 50) // 196 Problem https://mathworld.wolfram.com/196-Algorithm.html
                {
                    Console.WriteLine($"Encountered problem at: {temp}");

                    return;
                }
            }

        }

        private static bool Display(bool found, BigInteger temp, int numberOfSteps)
        {
            if (found)
            {
                Console.WriteLine($"{temp} -> {numberOfSteps} transformation{(numberOfSteps > 1 ? "s" : string.Empty)}");
            }

            return found;
        }

        private static BigInteger PerformInvert(BigInteger num)
        {
            num = num < 0 ? num * -1 : num;
            char[] charArray = num.ToString().ToCharArray();
            Array.Reverse(charArray);
            string result = string.Empty;

            foreach (var item in charArray)
            {
                result = $"{result}{item}";
            }

            return BigInteger.Parse(result);
        }

    }
}
